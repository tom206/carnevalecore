﻿using System;
using UnityEngine;
namespace Carnevale.UI
{
    /// <summary>
    /// A monobehaviour that attaches to a UI screen in order to have its life-cycle managed by itself and by the ScreenManager. 
    /// When the SceneManager queues a new screen, the old screen, if extant, will be asked to pop from the stack. When a screen is asked to pop, the
    /// manager waits for the CScreen to call onPopFinished, at which point the manager finally pops the CScreen from the stack
    /// and pushes this CScreen onto the stack, and waits for it to call onPushFinished before allowing any other screens to 
    /// push or pop the stack. This system of waiting for the CScreens to handle their lifecycles allows for each screen to
    /// handle transition effects and to clean themselves up so they can be properly disposed of.
    /// </summary>
    public abstract class CScreen : MonoBehaviour
    {
        /// <summary>
        /// A string id used to identify this screen, for instance with ScreenManager.GetScreen()
        /// </summary>
        /// <value></value>
        public string id { get; private set; }

        /// <summary>
        /// The name of this screen's prefab.
        /// </summary>
        /// <value></value>
        public string PrefabName { get; private set; }

        /// <summary>
        /// Is this screen at the top of the stack?
        /// </summary>
        /// <value></value>
        public bool IsFocused { get; protected set; }
        
        /// <summary>
        /// A delegate for the push and pop events
        /// </summary>
        /// <param name="screen">The screen being pushed or popped (this screen)</param>
        public delegate void ScreenDelegate(CScreen screen);

        /// <summary>
        /// The event the manager subscribes to for push events. The manager waits for this to be called to continue it's queue.
        /// </summary>
        public event ScreenDelegate onPushFinished;

        /// <summary>
        /// The event the manager subscribes to for pop events. The manager waits for this to be called to continue it's queue.
        /// </summary>
        public event ScreenDelegate onPopFinished;

        /// <summary>
        /// Called by the manager to set the properties of this screen
        /// </summary>
        /// <param name="id"></param>
        /// <param name="prefabName"></param>
        public void Setup(string id, string prefabName)
        {
            this.id = id;
            PrefabName = prefabName;
            IsFocused = false;
            OnSetup();
        }


        /// <summary>
        /// Setup is called after instantiating a Screen prefab. It is only called once for the lifecycle of the Screen.
        /// </summary>
        public abstract void OnSetup();

        /// <summary>
        /// Called by the UIManager when this Screen is being pushed to the screen stack.
        /// Be sure to call PushPopFinished when your screen is done pushing. Delaying the PushPopFinished call
        /// allows the screen to delay execution of the UIManager's screen queue.
        /// </summary>
        public abstract void OnPush();

        /// <summary>
        /// Called by the UIManager when this Screen is being popped from the screen stack.
        /// Be sure to call PopFinished when your screen is done popping. Delaying the PushPopFinished call
        /// allows the screen to delay execution of the UIManager's screen queue.
        /// </summary>
        public abstract void OnPop();

        /// <summary>
        /// Called by the UIManager when this Screen becomes the top most screen in the stack.
        /// </summary>
        public abstract void OnFocus();

        /// <summary>
        /// Called by the UIManager when this Screen is no longer the top most screen in the stack.
        /// </summary>
        public abstract void OnFocusLost();

        /// <summary>
        /// A wrapper for the onPushFinished event
        /// </summary>
        protected void PushFinished()
        {
            if (onPushFinished != null)
                onPushFinished(this);
        }

        /// <summary>
        /// A warpper for the onPopFinished event
        /// </summary>
        protected void PopFinished()
        {
            if (onPopFinished != null)
                onPopFinished(this);
        }


    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Carnevale.UI
{
    /// <summary>
    /// Handles the UI for a canvas in the scene by way of using Screens. Screens are abstract monobehaviours that should be attached to a prefab in Resources/UI/
    /// When you Queue an item, it checks if the system is ready, then pushes that Screen to the stack (on top), calling "OnPush" on the screen. The UIManager
    /// is now in the state of Push. Only after the Screen calls "PushFinished" will the callback be fired here to complete the Push, and ready the Manager for
    /// for items in the queue. This allows screens to handle animations and other asynchronous clean up tasks before telling the UI manager that they are done.
    /// All screens on the stack are visible so handle it accordingly. A screen is not popped if a screen is pushed
    /// to the top of the stack, only when that screen is told to be popped. The top screen is considered "Focused" and thus newly pushed screens will tell the
    /// old screen below it that it has lost focus.
    /// </summary>
    public class ScreenManager : Singleton<ScreenManager>
    {
        /// <summary>
        /// A base class to hold data in the queue
        /// </summary>
        private abstract class QueuedScreen
        {
            public string id;
        }

        /// <summary>
        /// An object that holds some data for a screen about to be pushed so we can store it in the queue
        /// </summary>
        private class QueuedScreenPush : QueuedScreen
        {
            public string prefabName;
            public PushedDelegate callback;

            public override string ToString()
            {
                return string.Format("[Push] {0}", id);
            }
        }

        /// <summary>
        /// An object that holds some data for a screen about to be popped so we can store it in the queue
        /// </summary>
        private class QueuedScreenPop : QueuedScreen
        {
            public PoppedDelegate callback;

            public override string ToString()
            {
                return string.Format("[Pop] {0}", id);
            }
        }

        /// <summary>
        /// Delegate for pushing a screen
        /// </summary>
        /// <param name="screen"></param>
        public delegate void PushedDelegate(CScreen screen);

        /// <summary>
        /// Delegate for popping a screen
        /// </summary>
        /// <param name="id"></param>
        public delegate void PoppedDelegate(string id);

        /// <summary>
        /// The current state of our manager
        /// </summary>
        private State _state = State.Uninitialized;
        private enum State
        {
            Uninitialized,
            Ready,
            Push,
            Pop
        }

        /// <summary>
        /// The queue for screens to be pushed and popped to and from the stack.
        /// First in, first out
        /// </summary>
        private Queue<QueuedScreen> _queue;

        /// <summary>
        /// The stack of screens on the UI. Only the top is visible
        /// </summary>
        private List<CScreen> _stack;

        /// <summary>
        /// Where we're loading prefabs from
        /// </summary>
        [SerializeField]
        private string _resourceDirectory = "UI/";

        /// <summary>
        /// The root canvas to attach screens to
        /// </summary>
        [SerializeField]
        private Canvas _rootCanvas;

        /// <summary>
        /// The callback to be fired when a queued push item is finished
        /// </summary>
        private PushedDelegate _activePushCallback;

        /// <summary>
        /// The callback to be fired when a queued pop item is finished
        /// </summary>
        private PoppedDelegate _activePopCallback;

        /// <summary>
        /// Whether or not we initialize when the behaviour awakes
        /// </summary>
        [SerializeField]
        public bool InitOnAwake = true;

        /// <summary>
        /// Singleton awake.
        /// </summary>
        protected override void SingletonAwake()
        {
            _queue = new Queue<QueuedScreen>();
            _stack = new List<CScreen>();

            if (InitOnAwake)
                Initialize();
        }


        /// <summary>
        /// Sets up and returns the success of the setup process
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task<bool> Initialize(string resourceDirectory = "UI/")
        {

            if (_rootCanvas == null)
            {
                Debug.LogError("[UIManager] _rootCanvas is null, initialization failed");
                return false;
            }

            DontDestroyOnLoad(_rootCanvas.transform);
            _state = State.Ready;
            return true;
        }

        /// <summary>
        /// Queues a screen to be pushed onto the stack
        /// </summary>
        /// <param name="id">the name of the screen</param>
        /// <param name="prefabName">the name of the prefab</param>
        /// <param name="callback">the callback to be fired when the push is complete</param>
        public void QueuePush(string id, string prefabName, PushedDelegate callback)
        {
            Debug.LogFormat("Queueing screen {0}, there are {1} items in the queue and {2} items in the stack", id, _queue.Count, _stack.Count);
            QueuedScreenPush push = new QueuedScreenPush();
            push.id = id;
            push.prefabName = prefabName;
            push.callback = callback;

            _queue.Enqueue(push);

            if (CanExecuteNextQueue())
                ExecuteNextQueueItem();
        }

        /// <summary>
        /// Queues an item to be popped from the stack, removing it and destroying it
        /// </summary>
        /// <param name="callback">The callback to fire when the screen has finished popping</param>
        public void QueuePop(PoppedDelegate callback)
        {
            CScreen topScreen = GetTopScreen();
            if (topScreen == null)
                return;

            QueuedScreenPop pop = new QueuedScreenPop();
            pop.id = topScreen.id;
            pop.callback = callback;

            _queue.Enqueue(pop);

            if (CanExecuteNextQueue())
                ExecuteNextQueueItem();
        }

        /// <summary>
        /// The Update method must be controlled by another manager, it will not call itself
        /// </summary>
        public void Update()
        {
            if (CanExecuteNextQueue())
                ExecuteNextQueueItem();
            else
            {


            }
        }

        /// <summary>
        /// Gets the top screen from the stack
        /// </summary>
        /// <returns></returns>
        public CScreen GetTopScreen()
        {
            if (_stack.Count > 0)
                return _stack[0];

            return null;
        }

        /// <summary>
        /// Gets a screen by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CScreen GetScreen(string id)
        {
            int count = _stack.Count;
            for (int i = 0; i < count; i++)
            {
                if (_stack[i].id == id)
                    return _stack[i];
            }

            return null;
        }

        /// <summary>
        /// Gets a screen by ID and returns it as the given type
        /// </summary>
        /// <param name="id"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetScreen<T>(string id) where T : CScreen
        {
            CScreen screen = GetScreen(id);
            return (T)screen;
        }

        /// <summary>
        /// Whether or not we can excute the next item
        /// </summary>
        /// <returns></returns>
        private bool CanExecuteNextQueue()
        {
            if (_state == State.Ready)
            {
                if (_queue.Count > 0)
                {
                    return true;
                }
            }

            return false;

        }

        /// <summary>
        /// whether or not the manager has been initialized.
        /// </summary>
        /// <returns></returns>
        public bool IsInitialized()
        {
            return _state != State.Uninitialized;
        }

        /// <summary>
        /// Executes the next queued item
        /// </summary>
        private void ExecuteNextQueueItem()
        {
            // Get next queued item.
            QueuedScreen queued = _queue.Dequeue();

            if (queued is QueuedScreenPush)
            {
                // Push screen.
                QueuedScreenPush queuedPush = (QueuedScreenPush)queued;
                CScreen screenInstance;

                // Instantiate new instance of screen.
                string path = System.IO.Path.Combine(_resourceDirectory, queuedPush.prefabName);
                CScreen prefab = Resources.Load<CScreen>(path);

                screenInstance = Object.Instantiate(prefab, _rootCanvas.transform);
                screenInstance.Setup(queuedPush.id, queuedPush.prefabName);


                // Tell previous top screen that it is losing focus.
                var topScreen = GetTopScreen();
                if (topScreen != null)
                {
                    topScreen.OnFocusLost();
                }

                // Insert new screen at the top of the stack.
                _state = State.Push;
                _stack.Insert(0, screenInstance);

                _activePushCallback = queuedPush.callback;

                screenInstance.onPushFinished += HandlePushFinished;
                screenInstance.OnPush();

                if (_queue.Count == 0)
                {
                    // Screen gains focus when it is on top of the screen stack and no other items in the queue.
                    screenInstance.OnFocus();
                }
            }
            else
            {
                // Pop screen.
                QueuedScreenPop queuedPop = (QueuedScreenPop)queued;
                CScreen screenToPop = GetTopScreen();

                if (screenToPop.id != queued.id)
                {
                    throw new System.Exception(string.Format("The top screen does not match the queued pop. " +
                                                             "TopScreen: {0}, QueuedPop: {1}", screenToPop.id, queued.id));
                }

                screenToPop.OnFocusLost();

                _state = State.Pop;
                _stack.RemoveAt(0);

                // Tell new top screen that it is gaining focus.
                var newTopScreen = GetTopScreen();
                if (newTopScreen != null)
                {
                    if (_queue.Count == 0)
                    {
                        // Screen gains focus when it is on top of the screen stack and no other items in the queue.
                        newTopScreen.OnFocus();
                    }
                }

                _activePopCallback = queuedPop.callback;

                screenToPop.onPopFinished += HandlePopFinished;
                screenToPop.OnPop();
            }
        }

        /// <summary>
        /// Called when a screen has finished pushing to the stack.
        /// </summary>
        /// <param name="screen"></param>
        private void HandlePushFinished(CScreen screen)
        {
            screen.onPushFinished -= HandlePushFinished;

            _state = State.Ready;

            if (_activePushCallback != null)
            {
                _activePushCallback(screen);
                _activePushCallback = null;
            }

            if (CanExecuteNextQueue())
                ExecuteNextQueueItem();
        }

        /// <summary>
        /// called when a screen has finished popping to the stack.
        /// </summary>
        /// <param name="screen"></param>
        private void HandlePopFinished(CScreen screen)
        {
            screen.onPopFinished -= HandlePopFinished;

            // Destroy screen.
            Object.Destroy(screen.gameObject);


            _state = State.Ready;

            if (_activePopCallback != null)
            {
                _activePopCallback(screen.id);
                _activePopCallback = null;
            }

            if (CanExecuteNextQueue())
                ExecuteNextQueueItem();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using Carnevale.UI;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScreen : CScreen
{
    private Image image;
    private Text text;
    private bool fadeOut = false;
    private float timer = 0;

    public override void OnFocus()
    {

    }

    public override void OnFocusLost()
    {

    }

    public override void OnPop()
    {
        fadeOut = true;
    }

    public override void OnPush()
    {
        PushFinished();
    }

    public override void OnSetup()
    {
        image = GetComponentInChildren<Image>();
        text = GetComponentInChildren<Text>();
    }

    public void Update()
    {
        if (fadeOut)
        {
            timer += Time.deltaTime;
            image.color = new Color(0, 0, 0, image.color.a * .99f);
            text.color = new Color(1, 1, 1, image.color.a);
            if (timer > 1)
            {
                PopFinished();
            }
        }
    }
}

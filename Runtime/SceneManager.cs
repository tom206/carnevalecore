﻿using System.Collections;
using System.Collections.Generic;
using Carnevale.UI;
using UnityEngine;
namespace Carnevale
{
    /// <summary>
    /// Asynchronously loads a new scene and uses the UI manager to put a loading screen up
    /// </summary>
    public class SceneManager : Singleton<SceneManager>
    {
        [SerializeField]
        public string LoadingScreenPrefabName = "LoadingScreen";

        private ScreenManager screenManager
        {
            get { return Singleton<ScreenManager>.Instance; }
        }
        
        private string _sceneToLoad = "";

        protected override void SingletonAwake()
        {
            DontDestroyOnLoad(gameObject);
        }

        public void LoadScene(string sceneName)
        {
            if (screenManager.IsInitialized() == false)
            {
                Debug.LogError("[SceneManager] Scene manager requires an initialized UIManager.");
            }

            _sceneToLoad = sceneName;
            if (!string.IsNullOrEmpty(LoadingScreenPrefabName))
                screenManager.QueuePush("loading", LoadingScreenPrefabName, StartLoad);
            else
                StartLoad(null);
        }

        private void StartLoad(CScreen screen)
        {
            Debug.Log("[SceneManager] Start load " + _sceneToLoad);
            if (!string.IsNullOrEmpty(_sceneToLoad))
                StartCoroutine(LoadSceneAsync(_sceneToLoad));
        }

        private IEnumerator LoadSceneAsync(string sceneName)
        {
            Debug.Log("[SceneManager] Load scene Async " + _sceneToLoad);
            yield return null;
            var asyncLoad = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName);
            // Wait until the asynchronous scene fully loads
            while (!asyncLoad.isDone)
            {
                yield return null;
            }
            Debug.Log("[SceneManager] Scene loaded");
            screenManager.QueuePop(null);
            _sceneToLoad = "";
        }


    }

}
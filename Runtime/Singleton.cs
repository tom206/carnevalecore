using UnityEngine;
using System.Collections;
using System;

namespace Carnevale
{
    /// <summary>
    /// A singleton for MonoBehaviour based classes. Singleton will initialize on the first Instance get request or on Awake, whichever happens first.
    /// This singleton is intended for a MonoBehaviour that is designed to act like a static object (like a GameManager).
    /// </summary>
    public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        private static T _Instance;

        private bool _awakened;

        public static T Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = FindObjectOfType<T>();

                    if (_Instance == null)
                    {
                        _Instance = GenerateInstance();
                    }

                    if (_Instance != null)
                    {
                        if (!_Instance._awakened)
                        {
                            _Instance.SingletonAwake();
                            _Instance._awakened = true;
                        }
                    }
                }

                return _Instance;
            }
        }

        protected static T GenerateInstance()
        {
            var instanceGameObject = new GameObject(typeof(T).ToString() + "_Singleton");
            return instanceGameObject.AddComponent<T>();
        }

        private void Awake()
        {
            if (_Instance == this)
                return;

            if (_Instance == null)
            {
                _Instance = (T)this;

                if (!_Instance._awakened)
                {
                    _Instance.SingletonAwake();
                    _Instance._awakened = true;
                }
            }
            else
            {
                Debug.LogException(new System.Exception("Duplicate singleton found for " + typeof(T).Name + "."), gameObject);
            }
        }

        /// <summary>
        /// Replaces the MonoBehaviour Awake call. Since MonoBehaviourSingleton are not newly constructed objects, use this initialization method
        /// to setup the singleton object as needed when the singleton is initialized.
        /// </summary>
        protected abstract void SingletonAwake();
    }
}
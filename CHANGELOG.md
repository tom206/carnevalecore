# Changelog


## [0.0.2] - 2020-03-02

 - Renamed UIManager to SceneManager
 - Removed Examples from package


## [0.0.1] - 2020-02-26

 - Created Package